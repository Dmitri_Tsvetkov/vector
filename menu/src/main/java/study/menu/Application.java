package study.menu;

import java.util.Scanner;
import study.vectorlib.Vector;

public class Application {
    public static void main(String[] args) {
        Vector vector1 = new Vector(8, 8, 2);
        Vector vector2 = new Vector(2, 3, 4);

        Scanner scanner = new Scanner(System.in);
        int menu;
        do {
            System.out.println("1.Длина вектора:");
            System.out.println("2.Сложение векторов:");
            System.out.println("3.Вычитание векторов: ");
            System.out.println("4.Умножение на число: ");
            System.out.println("5.Векторное произведение векторов:");
            System.out.println("6.Скалярное произведение: ");
            menu = scanner.nextInt();
            switch (menu) {
                case 1: {
                    double length = vector1.length();
                    System.out.println("Длина вектора:");
                    System.out.println(length);
                    break;

                }
                case 2: {
                    Vector result = vector1.add(vector2);

                    System.out.println("Сложение векторов:");
                    System.out.print(result.getX() + ", ");
                    System.out.print(result.getY() + ", ");
                    System.out.println(result.getZ());
                    break;

                }
                case 3: {
                    Vector result1 = vector1.sub(vector2);

                    System.out.println("Вычитание векторов: ");
                    System.out.print(result1.getX() + ", ");
                    System.out.print(result1.getY() + ", ");
                    System.out.println(result1.getZ());
                    break;

                }
                case 4: {
                    int number = 3;
                    Vector result2 = vector1.multiplication(number);

                    System.out.println("Умножение на число: ");
                    System.out.print(result2.getX() + ", ");
                    System.out.print(result2.getY() + ", ");
                    System.out.println(result2.getZ());
                    break;
                }
                case 5: {
                    Vector result3 = vector1.vectorProduct(vector2);

                    System.out.println("Векторное произведение векторов:");
                    System.out.print(result3.getX() + ", ");
                    System.out.print(result3.getY() + ", ");
                    System.out.println(result3.getZ());
                    break;
                }
                case 6: {
                    double result4 = vector1.scalarVector(vector2);

                    System.out.println("Скалярное произведение: ");
                    System.out.print(result4);
                    break;
                }

            }

        }
        while ((menu == 1) || (menu == 2) || (menu == 3) || (menu == 4) || (menu == 5) || (menu == 6));
    }
}

