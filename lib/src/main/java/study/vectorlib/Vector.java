package study.vectorlib;

public class Vector {
    private double x, y, z, length;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        length = Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public double length() {
        return length;
    }

    public Vector add(Vector vector) {
        double x = this.x + vector.x;
        double y = this.y + vector.y;
        double z = this.z + vector.z;
        return new Vector(x, y, z);
    }

    public Vector sub(Vector vector) {
        double x = this.x - vector.x;
        double y = this.y - vector.y;
        double z = this.z - vector.z;
        return new Vector(x, y, z);
    }

    public Vector multiplication(double number) {
        double x = this.x * number;
        double y = this.y * number;
        double z = this.z * number;
        return new Vector(x, y, z);
    }

    public Vector vectorProduct(Vector vector) {
        double x = this.y * vector.z - this.z * vector.y;
        double y = this.z * vector.x - this.x * vector.z;
        double z = this.x * vector.y - this.y * vector.x;
        return new Vector(x, y, z);
    }

    public double scalarVector(Vector vector) {
        double x = this.x * vector.x;
        double y = this.y * vector.y;
        double z = this.z * vector.z;
        return x + y + z;
    }

    public Vector turnByXDeg(double deg) {
        double rad = Math.toRadians(deg);
        return turnByXRad(rad);
    }

    private Vector turnByXRad(double rad) {
        double x = this.x;
        double y = this.y * Math.cos(rad) - this.z * Math.sin(rad);
        double z = this.y * Math.sin(rad) + this.z * Math.cos(rad);
        return new Vector(x, y, z);
    }

    public Vector turnByZDeg(double deg) {
        double rad = Math.toRadians(deg);
        return turnByZRad(rad);
    }

    private Vector turnByZRad(double rad) {
        double x = this.x * Math.cos(rad) - this.y * Math.sin(rad);
        double y = this.x * Math.sin(rad) + this.y * Math.cos(rad);
        double z = this.z;
        return new Vector(x, y, z);
    }

    public Vector turnByYDeg(double deg) {
        double rad = Math.toRadians(deg);
        return turnByYRad(rad);
    }

    private Vector turnByYRad(double rad) {
        double x = this.z * Math.sin(rad) + this.x * Math.cos(rad);
        double y = this.y;
        double z = this.z * Math.cos(rad) - this.x * Math.sin(rad);
        return new Vector(x, y, z);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

}

