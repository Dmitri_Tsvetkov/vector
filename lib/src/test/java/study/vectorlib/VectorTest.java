package study.vectorlib;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class VectorTest {
    @Test
    public void length() {
        Vector vector = new Vector(2, 2, 1);

        double length = vector.length();

        Assert.assertEquals(3, length, 0.00001);
    }

    @Test
    public void add() {
        Vector vector1 = new Vector(6, 7, 8);
        Vector vector2 = new Vector(5, 1, -3);

        Vector result = vector1.add(vector2);

        Assert.assertEquals(11, result.getX(), 0.00001);
        Assert.assertEquals(8, result.getY(), 0.00001);
        Assert.assertEquals(5, result.getZ(), 0.00001);
    }

    @Test
    public void sub() {
        Vector vector1 = new Vector(9, 5, 12);
        Vector vector2 = new Vector(4, 3, 7);

        Vector result = vector1.sub(vector2);

        Assert.assertEquals(5, result.getX(), 0.00001);
        Assert.assertEquals(2, result.getY(), 0.00001);
        Assert.assertEquals(5, result.getZ(), 0.00001);
    }

    @Test
    public void multiplication() {
        Vector vector1 = new Vector(2, 3, 5);

        int number = 3;
        Vector result = vector1.multiplication(number);

        Assert.assertEquals(6, result.getX(), 0.00001);
        Assert.assertEquals(9, result.getY(), 0.00001);
        Assert.assertEquals(15, result.getZ(), 0.00001);
    }

    @Test
    public void vectorProduct() {
        Vector vector1 = new Vector(5, 3, 9);
        Vector vector2 = new Vector(6, 7, 8);

        Vector result = vector1.vectorProduct(vector2);

        Assert.assertEquals(-39, result.getX(), 0.00001);
        Assert.assertEquals(14, result.getY(), 0.00001);
        Assert.assertEquals(17, result.getZ(), 0.00001);

    }

    @Test
    public void scalarVector() {
        Vector vector1 = new Vector(4, -3, 1);
        Vector vector2 = new Vector(5, -2, -3);

        double result = vector1.scalarVector(vector2);

        Assert.assertEquals(23, result, 0.00001);
    }

    @Test
    public void turnByZDeg() {
        Vector vector = new Vector(1, 1, 0);
        Vector result = vector.turnByZDeg(45);

        double expected = Math.sqrt(2);

        Assert.assertEquals(0, result.getX(), 0.00001);
        Assert.assertEquals(expected, result.getY(), 0.00001);
    }

    @Test
    public void turnByYDeg() {
        Vector vector = new Vector(1, 0, 1);
        Vector result = vector.turnByYDeg(45);

        double expected = Math.sqrt(2);

        Assert.assertEquals(0, result.getZ(), 0.00001);
        Assert.assertEquals(expected, result.getX(), 0.00001);
    }

    @Test
    public void turnByXDeg() {
        Vector vector = new Vector(0, 1, 1);
        Vector result = vector.turnByXDeg(45);

        double expected = Math.sqrt(2);

        Assert.assertEquals(0, result.getY(), 0.00001);
        Assert.assertEquals(expected, result.getZ(), 0.00001);
    }
}